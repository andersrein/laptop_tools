#include <string.h>
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include <unistd.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/time.h>
#include <fcntl.h>
#include <pthread.h>

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

#include <wayland-client.h>

#include "xdg-shell-client-protocol.h"
#include "argumentor.h"

#if 1
#define TRACE(fmt, args...) fprintf(stderr, "TRACE: " fmt "\n", ##args);
#else
#define TRACE(fmt, args...)
#endif

#define WL_CHECK(x) { \
    int ret = x; \
    if (ret == -1) { \
        fprintf( \
            stderr, \
            "ERROR: Wayland call %s failed with return code %d.\n", \
            #x, \
            ret \
        ); \
        exit(1); \
    } \
}

struct context_t {
    struct wl_display* display;
    struct wl_registry* registry;
    struct wl_compositor* compositor;
    struct xdg_wm_base* xdg_wm_base;
    struct wl_surface* surface;
    struct wl_shm* shm;
    struct wl_shm_pool* shm_pool;
    struct wl_buffer* buffer;
    struct xdg_surface* xdg_surface;
    struct xdg_toplevel* xdg_toplevel;
    struct xdg_positioner* xdg_positioner;
    int terminated;
};

static void no_op( ) { }

static void add_supported_format(
    void* opaque,
    struct wl_shm* shm,
    uint32_t format
) {
    char format_name[5];
    memset(&format_name[0], 0, 5);
    const char* src = (const char*)&format;

    int alpha_num = 0;
    for (int i = 0; i < 4; i++) {
        format_name[i] = src[i];

        alpha_num += (src[i] >= '0') && (src[i] <= 'z');
    }

    if (alpha_num != 4) {
        if (format == 0) {
            TRACE("Supported format: ARGB8888");
        } else if (format == 1) {
            TRACE("Supported format: XRGB8888");
        } else {
            TRACE("Supported format: %d", format);
        }
    } else {
        TRACE("Supported format: %s", format_name);
    }
}

const struct wl_shm_listener shm_listener = {
    .format = add_supported_format,
};
static void configure_xdg_surface(
    void* opaque,
    struct xdg_surface* xdg_surface,
    uint32_t serial
) {
    struct context_t* ctx = (struct context_t*)opaque;
    xdg_surface_ack_configure(xdg_surface, serial);
    wl_surface_commit(ctx->surface);
}

const struct xdg_surface_listener xdg_surface_listener = {
    .configure = configure_xdg_surface,
};

static void xdg_ping(
    void* opqaue,
    struct xdg_wm_base* xdg_wm_base,
    uint32_t serial
) {
    TRACE("Ping-Pong");
    xdg_wm_base_pong(xdg_wm_base, serial);
}

const struct xdg_wm_base_listener xdg_wm_base_listener = {
    .ping = xdg_ping,
};

static void on_close(
    void* opaque,
    struct xdg_toplevel* xdg_toplevel
) {
    struct context_t* ctx = (struct context_t*)opaque;
    ctx->terminated = 1;
}

const struct xdg_toplevel_listener xdg_toplevel_listener = {
    .configure = no_op,
    .configure_bounds = no_op,
    .wm_capabilities = no_op,
    .close = on_close
};

static void register_global(
    void* opaque,
    struct wl_registry* registry,
    uint32_t name,
    const char* interface,
    uint32_t version
) {
    struct context_t* ctx = (struct context_t*)opaque;

    TRACE("Register global interface: %s", interface);
    if (strcmp(wl_compositor_interface.name, interface) == 0) {
        ctx->compositor =
            wl_registry_bind(
                registry,
                name,
                &wl_compositor_interface,
                version
            );
        TRACE("Compositor registered");
    }

    if (strcmp(xdg_wm_base_interface.name, interface) == 0) {
        ctx->xdg_wm_base =
            wl_registry_bind(
                registry,
                name,
                &xdg_wm_base_interface,
                version
            );


        xdg_wm_base_add_listener(
            ctx->xdg_wm_base,
            &xdg_wm_base_listener,
            opaque
        );

        TRACE("XDG Shell registered");
    }

    if (strcmp(wl_shm_interface.name, interface) == 0) {
        ctx->shm =
            wl_registry_bind(
                registry,
                name,
                &wl_shm_interface,
                1
            );

        wl_shm_add_listener(
            ctx->shm,
            &shm_listener,
            opaque
        );

        TRACE("SHM registered");
    }
};

static void release_buffer(
    void *data,
    struct wl_buffer* wl_buffer
) {
    TRACE("Releasing buffer");

    struct context_t* ctx = (struct context_t*)data;
    ctx->buffer = wl_buffer;
}

const struct wl_buffer_listener buffer_listener = {
    .release = release_buffer,
};

const struct wl_registry_listener registry_listener = {
    .global = register_global,
    .global_remove = no_op,
};

int64_t elapsed_us(struct timeval* tv) {
    struct timeval a;
    assert(gettimeofday(&a, NULL) == 0);

    int64_t secdiff = (a.tv_sec - tv->tv_sec);
    int64_t usecdiff = (a.tv_usec - tv->tv_usec);
    int64_t diff = (secdiff * 1000000) + usecdiff;

    memcpy(tv, &a, sizeof(struct timeval));

    return diff;
}

struct icon_state_t {
    char shm_name[1024];
    int ref_count;
    int64_t timer;
    int close;
    int total;
    int value;
    pthread_mutex_t mutex;
};

static const char* shm_name_from_icon_path(const char* icon_path) {
    const char* shm_name = icon_path;
    int i = 0;
    while (icon_path[i] != 0) {
        if (icon_path[i] == '/') {
            shm_name = icon_path + 1 + i;
        }
        i++;
    }
    return shm_name;
}


struct icon_state_t* create_icon_state(const char* icon_path) {
    const char* shm_name = shm_name_from_icon_path(icon_path);
    TRACE("Creating icon state: %s", shm_name);

    size_t size = sizeof(struct icon_state_t);
    int fd = shm_open(shm_name, O_RDWR | O_CREAT, 0664);

    if (fd < 0) {
        fprintf(stderr, "ERROR: Failed to create SHM\n");
        exit(1);
    }

    if (ftruncate(fd, size) < 0) {
        fprintf(stderr, "ERROR: Failed to size SHM\n");
        exit(1);
    }

    struct icon_state_t* state =
        mmap(
            NULL,
            size,
            PROT_READ | PROT_WRITE,
            MAP_SHARED,
            fd,
            0
        );

    memset(state, 0, sizeof(struct icon_state_t));
    strcpy(&state->shm_name[0], shm_name);
    state->ref_count ++;
    pthread_mutexattr_t attr;
    pthread_mutexattr_init(&attr);
    pthread_mutexattr_setpshared(&attr, PTHREAD_PROCESS_SHARED);
    pthread_mutex_init(&state->mutex, &attr);
    pthread_mutexattr_destroy(&attr);

    close(fd);

    return state;
}

struct icon_state_t* load_icon_state(const char* icon_path) {
    const char* shm_name = shm_name_from_icon_path(icon_path);
    TRACE("Looking for existing icon_state: %s", shm_name);
    int fd = shm_open(shm_name, O_RDWR, 0664);

    if (fd < 0) {
        TRACE("Icon state does not exist");
        return NULL;
    }

    size_t size = sizeof(struct icon_state_t);

    struct icon_state_t* state =
        mmap(
            NULL,
            size,
            PROT_READ | PROT_WRITE,
            MAP_SHARED,
            fd,
            0
        );

    state->ref_count ++;

    close(fd);

    return state;
}

void release_icon_state(struct icon_state_t** state) {
    if (state == 0 || ((*state) == 0)) {
        return;
    }

    (*state)->ref_count--;

    if ((*state)->ref_count <= 0) {
        TRACE("Cleaning up shared memory");
        shm_unlink((*state)->shm_name);
    } else {
        TRACE("SHM ref count: %d", (*state)->ref_count);
    }

    munmap(*state, sizeof(struct icon_state_t*));
}

struct options_t {
    int timeout_ms;
    const char* icon_path;
    int terminate;
    int value;
    int total;
};

void icon_state_update(
    struct icon_state_t* state,
    struct options_t* options
) {
    if (options->timeout_ms != 0) {
        state->timer = (options->timeout_ms * 1000);
    }

    if (options->terminate == 1) {
        state->close = 1;
    }

    state->total = options->total;
    state->value = options->value;
}

struct stb_pixel_t {
    uint8_t r;
    uint8_t b;
    uint8_t g;
    uint8_t a;
};

struct wl_pixel_t {
    uint8_t b;
    uint8_t g;
    uint8_t r;
    uint8_t a;
};


void context_draw_window(
    struct context_t* ctx,
    struct wl_pixel_t* dst,
    const struct stb_pixel_t *src,
    int width,
    int height,
    int value,
    int total
) {

    struct wl_pixel_t* p = dst;
    for (int i = 0; i < height; i++) {
        for (int j= 0; j < width; j++) {
            p->r = src->r;
            p->g = src->g;
            p->b = src->b;
            p->a = src->a;
            p++;
            src++;
        }
    }

    if (total != 0) {
        const int pad = 40;
        const int bar_height = 20;

        int x1 = pad;
        int y1 = height - pad - bar_height;
        int x2 = width - pad;
        int y2 = height - pad;

        int sep =
            ((value * (x2 - x1)) / total) + x1;

        for (int i = y1; i <= y2; i++) {
            struct wl_pixel_t* row = dst + (i * width) + x1;

            for (int j = x1; j <= x2; j++) {
                int white =
                    (j == x1) ||
                    (j == x2) ||
                    (i == y1) ||
                    (i == y2) ||
                    (j < sep);

                if (white == 1) {
                    row->r = 255;
                    row->g = 255;
                    row->b = 255;
                }
                row++;
            }
        }
    }

    wl_surface_attach(ctx->surface, ctx->buffer, 0, 0);
    ctx->buffer = NULL;

    TRACE("Damaging buffer");
    wl_surface_damage_buffer(ctx->surface, 0, 0, width, height);

    TRACE("Committing buffer");
    wl_surface_commit(ctx->surface);

}

int main(int argc, char* argv[]) {
    TRACE("Starting wl_icon");

    struct options_t options;
    options.timeout_ms = 0;
    options.icon_path = NULL;
    options.terminate = 0;
    options.value = 0;
    options.total = 0;

    argumentor_t parser = NULL;
    argumentor_init(&parser);
    argumentor_register_int(
        parser,
        "--timeout",
        "-t",
        "Set timeout before icon disappears.",
        &options.timeout_ms
    );
    argumentor_register_string(
        parser,
        "icon",
        NULL,
        "Filename to a PNG to use as icon. Also SHM ID",
        &options.icon_path
    );
    argumentor_register_switch(
        parser,
        "--remove",
        "-R",
        "Immediately remove the displayed icon",
        &options.terminate
    );
    argumentor_register_int(
        parser,
        "--total",
        "-T",
        "Enable progress par and set max value",
        &options.total
    );
    argumentor_register_int(
        parser,
        "--value",
        "-V",
        "Set the value for the progress bar",
        &options.value
    );
    argumentor_parse(
        parser,
        argc,
        argv
    );
    argumentor_free(&parser);

    struct icon_state_t* icon =
        load_icon_state(options.icon_path);

    if (icon) {
        TRACE("Sending shared memory to existing process");

        pthread_mutex_lock(&icon->mutex);

        icon_state_update(icon, &options);

        pthread_mutex_unlock(&icon->mutex);

        release_icon_state(&icon);

        TRACE("Done");

        return 0;
    } else {
        if (options.terminate) {
            TRACE("Removing non existant icon.");
            return 0;
        }
        icon = create_icon_state(options.icon_path);

        icon_state_update(icon, &options);
    }

    int width = 0;
    int height = 0;
    int component_count = 0;

    stbi_uc *icon_pixels =
        stbi_load(
            options.icon_path,
            &width,
            &height,
            &component_count,
            4
        );

    if (!icon_pixels) {
        fprintf(
            stderr,
            "ERROR: Failed to load image: %s\n",
            options.icon_path
        );
        exit(1);
    }

    if (component_count != 4) {
        fprintf(stderr, "ERROR: Invalid pixel format for image\n");
        exit(1);
    }


    struct context_t ctx;
    memset(&ctx, 0, sizeof(ctx));

    ctx.display = wl_display_connect(NULL);
    assert(ctx.display != NULL);

    ctx.registry = wl_display_get_registry(ctx.display);
    assert(ctx.registry != NULL);

    WL_CHECK(
        wl_registry_add_listener(
            ctx.registry,
            &registry_listener,
            &ctx
        )
    );

    WL_CHECK(wl_display_dispatch(ctx.display));
    WL_CHECK(wl_display_roundtrip(ctx.display));

    if (ctx.compositor == NULL) {
        fprintf(stderr, "ERROR: No compositor registered\n");
        exit(1);
    }

    if (ctx.xdg_wm_base == NULL) {
        fprintf(stderr, "ERROR: No XDG WM Base registered\n");
        exit(1);
    }

    if (ctx.shm == NULL) {
        fprintf(stderr, "ERROR: No shm registered\n");
        exit(1);
    }

    ctx.surface = wl_compositor_create_surface(ctx.compositor);
    assert(ctx.surface != 0);

    const uint32_t format = WL_SHM_FORMAT_ARGB8888;
    const uint32_t stride = width * 4;

    ctx.xdg_surface =
        xdg_wm_base_get_xdg_surface(ctx.xdg_wm_base, ctx.surface);
    xdg_surface_add_listener(ctx.xdg_surface, &xdg_surface_listener, &ctx);
    ctx.xdg_toplevel = xdg_surface_get_toplevel(ctx.xdg_surface);
    xdg_toplevel_set_title(ctx.xdg_toplevel, "wl_icon");
    xdg_toplevel_set_max_size(ctx.xdg_toplevel, width, height);
    xdg_toplevel_set_min_size(ctx.xdg_toplevel, width, height);
    xdg_toplevel_add_listener(
        ctx.xdg_toplevel,
        &xdg_toplevel_listener,
        &ctx
    );

    wl_surface_commit(ctx.surface);
    wl_display_roundtrip(ctx.display);

    const size_t buffer_size = (size_t)(stride * height);

    const size_t MAX_SHM_NAME_LENGTH = 1024;
    char shm_name[MAX_SHM_NAME_LENGTH];
    memset(&shm_name[0], 0, MAX_SHM_NAME_LENGTH);

    int pid = (int)getpid();

    if (sprintf(&shm_name[0], "/wl_icon_%d", pid) < 0) {
        fprintf(stderr, "ERROR: Failed to create SHM name\n");
        exit(1);
    }

    int fd = shm_open(&shm_name[0], O_RDWR | O_CREAT, 0664);

    if (fd < 0) {
        fprintf(stderr, "ERROR: Failed to open SHM file\n");
        exit(1);
    }

    if (ftruncate(fd, buffer_size) < 0) {
        fprintf(stderr, "ERROR: Failed to size SHM\n");
        exit(1);
    }

    struct wl_pixel_t* pixels =
        mmap(
            NULL,
            buffer_size,
            PROT_READ | PROT_WRITE,
            MAP_SHARED,
            fd,
            0
        );

    shm_unlink(&shm_name[0]);

    assert(pixels != NULL);

    ctx.shm_pool = wl_shm_create_pool(ctx.shm, fd, buffer_size);
    assert(ctx.shm_pool);

    ctx.buffer =
        wl_shm_pool_create_buffer(
            ctx.shm_pool,
            0,
            width,
            height,
            stride,
            format
        );
    assert(ctx.buffer);

    wl_buffer_add_listener(
        ctx.buffer,
        &buffer_listener,
        &ctx
    );

    TRACE("Painting window");

    context_draw_window(
        &ctx,
        pixels,
        (const struct stb_pixel_t*)icon_pixels,
        width,
        height,
        icon->value,
        icon->total
    );

    TRACE("Buffer committed");

    struct timeval current_time;
    elapsed_us(&current_time);

    TRACE("Locking mutex");
    pthread_mutex_lock(&icon->mutex);

    int current_total = 0;
    int current_value = 0;

    while ( 1 ) {
        pthread_mutex_unlock(&icon->mutex);

        while (wl_display_prepare_read(ctx.display) == -1) {
            wl_display_dispatch_pending(ctx.display);
        }

        wl_display_flush(ctx.display);
        wl_display_read_events(ctx.display);

        wl_display_dispatch_pending(ctx.display);

        pthread_mutex_lock(&icon->mutex);

        if (ctx.terminated == 1) {
            TRACE("Compositor requesting terminate");
            break;
        }

        int elapsed = elapsed_us(&current_time);
        if (icon->timer > 0) {
            icon->timer -= elapsed;
            if (icon->timer <= 0) {
                TRACE("Icon timed out");
                icon->close = 1;
            }
        }

        if (icon->close == 1) {
            break;
        }

        if ((ctx.buffer != NULL) &&
            (icon->total != 0) && (
            (icon->total != current_total) ||
            (icon->value != current_value)
        )) {
            TRACE(
                "Updating progress bar: [%d/%d]",
                icon->value,
                icon->total
            );

            current_total = icon->total;
            current_value = icon->value;

            context_draw_window(
                &ctx,
                pixels,
                (struct stb_pixel_t*)icon_pixels,
                width,
                height,
                icon->value,
                icon->total
            );

            TRACE("Surface updated");
        }
    }

    close(fd);

    pthread_mutex_unlock(&icon->mutex);

    release_icon_state(&icon);


    return 0;
}
