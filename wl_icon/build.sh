#!/bin/bash

set -e

if ! [ -d output ]; then
    mkdir output
fi

if ! which wayland-scanner &> /dev/null; then
  echo "ERROR: missing wayland-scanner. Install wayland-devel"
  exit 1
fi

if ! pkg-config wayland-protocols --variable=pkgdatadir &> /dev/null; then
  echo "ERROR: Missing wayland-protocols-devel"
  exit 1
fi

WL_PROT_DIR=$(pkg-config wayland-protocols --variable=pkgdatadir)
WL_XDG_PROT=${WL_PROT_DIR}/stable/xdg-shell/xdg-shell.xml

wayland-scanner client-header ${WL_XDG_PROT} output/xdg-shell-client-protocol.h
wayland-scanner private-code ${WL_XDG_PROT} output/xdg-shell-client-protocol.c

gcc -g -pthread $(cat compile_flags.txt) -Wall -Werror argumentor.c output/xdg-shell-client-protocol.c wl_icon.c -lm -lrt -lwayland-client -o output/wl_icon

echo "wl_icon built successfully"
