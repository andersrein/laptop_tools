#!/bin/bash

set -e

if ! [ -f output/wl_icon ]; then
    echo "Run ./build.sh first"
    exit 1
fi

cp output/wl_icon /usr/bin/wl_icon
