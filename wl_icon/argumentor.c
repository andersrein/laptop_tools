#include "argumentor.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

enum argument_type_t {
    ARGUMENT_TYPE_INT,
    ARGUMENT_TYPE_SWITCH,
    ARGUMENT_TYPE_STRING,
};

struct named_arg_entry_t {
    struct named_arg_entry_t* next;
    const char* name;
    const char* description;
    enum argument_type_t type;
    void* target;
};

struct positional_arg_entry_t {
    struct positional_arg_entry_t* next;
    const char* name;
    const char* description;
    enum argument_type_t type;
    void* target;
};

struct generic_list_entry_t {
    struct generic_list_entry_t* next;
};

void add_to_list(
    struct generic_list_entry_t** list,
    struct generic_list_entry_t* entry
) {
    if ((*list) == 0) {
        (*list) = entry;
    } else {
        struct generic_list_entry_t* e = (*list);

        while (e->next != 0) {
            e = e->next;
        }

        e->next = entry;
    }
}

void free_list(struct generic_list_entry_t** list) {
    struct generic_list_entry_t* e = (*list);

    while (e != 0) {
        struct generic_list_entry_t* n = e->next;
        free(e);
        e = n;
    }
    (*list) = 0;
}

struct argumentor_ST {
    struct named_arg_entry_t* named_list;
    struct positional_arg_entry_t* positional_list;
};

int argumentor_init(argumentor_t* ctx) {
    if (ctx == 0) {
        return -1;
    }

    if (*ctx != 0) {
        return -1;
    }

    size_t size = sizeof(struct argumentor_ST);
    (*ctx) = malloc(size);
    memset(*ctx, 0, size);

    return 0;
}

int register_argument_generic(
    argumentor_t ctx,
    const char* long_name,
    const char* short_name,
    const char* description,
    enum argument_type_t type,
    void* target
) {
    if (!description) {
        fprintf(stderr, "ERROR: description must be non NULL\n");
        return -1;
    }

    if (!target) {
        fprintf(stderr, "ERROR: target must be non NULL\n");
        return -1;
    }

    if (long_name[0] != '-') {
        if (short_name != 0) {
            fprintf(
                stderr,
                "ERROR: short_name must be NULL for positional "
                "arguments."
            );
            return -1;
        }
        size_t size = sizeof(struct positional_arg_entry_t);
        struct positional_arg_entry_t* entry = malloc(size);
        entry->next = 0;
        entry->name = long_name;
        entry->description = description;
        entry->type = type;
        entry->target = target;
        add_to_list(
            (struct generic_list_entry_t**)&(ctx->positional_list),
            (struct generic_list_entry_t*)entry
        );
    } else {
        size_t size = sizeof(struct named_arg_entry_t);

        if (short_name != NULL) {
            struct named_arg_entry_t* short_entry = malloc(size);
            short_entry->next = 0;
            short_entry->name = short_name;
            short_entry->type = type;
            short_entry->target = target;
            short_entry->description = NULL;
            add_to_list(
                (struct generic_list_entry_t**)&(ctx->named_list),
                (struct generic_list_entry_t*)short_entry
            );
        }

        struct named_arg_entry_t* long_entry = malloc(size);
        long_entry->next = 0;
        long_entry->name = long_name;
        long_entry->type = type;
        long_entry->target = target;
        long_entry->description = description;
        add_to_list(
            (struct generic_list_entry_t**)ctx->named_list,
            (struct generic_list_entry_t*)long_entry
        );
    }
    return 0;
}

int argumentor_register_int(
    argumentor_t ctx,
    const char* long_name,
    const char* short_name,
    const char* description,
    int* target
) {
    return register_argument_generic(
        ctx,
        long_name,
        short_name,
        description,
        ARGUMENT_TYPE_INT,
        target
    );
}

int argumentor_register_string(
    argumentor_t ctx,
    const char* long_name,
    const char* short_name,
    const char* description,
    const char** target
) {
    return register_argument_generic(
        ctx,
        long_name,
        short_name,
        description,
        ARGUMENT_TYPE_STRING,
        target
    );
}

int argumentor_register_switch(
    argumentor_t ctx,
    const char* long_name,
    const char* short_name,
    const char* description,
    int* target
) {
    return register_argument_generic(
        ctx,
        long_name,
        short_name,
        description,
        ARGUMENT_TYPE_SWITCH,
        target
    );
}

static int parse_value(
    const char* value_str,
    enum argument_type_t type,
    void* target
) {
    if (type == ARGUMENT_TYPE_INT) {
        if (sscanf(value_str, "%d", (int*)target) != 1) {
            return -1;
        }
    } else if (type == ARGUMENT_TYPE_STRING) {
        const char** s = (const char**)target;
        (*s) = value_str;
    } else {
        return -1;
    }

    return 0;
}

static const char* type_string(enum argument_type_t type) {
    switch (type) {
        case ARGUMENT_TYPE_INT: return "int";
        case ARGUMENT_TYPE_STRING: return "string";
        case ARGUMENT_TYPE_SWITCH: return 0;
    }
    return 0;
}

static void print_usage(char* executable, argumentor_t ctx) {
    fprintf(stdout, "Usage:\n");
    fprintf(stdout, "    %s", executable);

    struct named_arg_entry_t* n = ctx->named_list;

    while (n != 0) {
        if (n->description) {
            fprintf(stdout, " [%s", n->name);
            if (n->type != ARGUMENT_TYPE_SWITCH) {
                fprintf(stdout, "=<%s>", type_string(n->type));
            }
            fprintf(stdout, "]");
        }
        n = n->next;
    }

    struct positional_arg_entry_t* p = ctx->positional_list;
    while (p != 0) {
        fprintf(stdout, " <%s>", p->name);
        p = p->next;
    }
    fprintf(stdout, "\n\n");

    fprintf(stdout, "Positional arguments:\n\n");

    p = ctx->positional_list;
    while (p != 0) {
        fprintf(stdout, "    <%s>\n\n", p->name);
        fprintf(stdout, "        %s\n\n", p->description);
        p = p->next;
    }

    fprintf(stdout, "Options:\n\n");

    n = ctx->named_list;
    while (n != 0) {
        fprintf(stdout, "    %s", n->name);
        if (n->type != ARGUMENT_TYPE_SWITCH) {
            fprintf(stdout, "=<%s>", type_string(n->type));
        }
        fprintf(stdout, "\n");

        if (n->description) {
            fprintf(stdout, "\n        %s\n\n", n->description);
        }

        n = n->next;
    }
}

int argumentor_parse(argumentor_t ctx, int argc, char** argv) {
    char* executable = argv[0];

    struct positional_arg_entry_t* pos = ctx->positional_list;

    int pos_index = 1;

    for (int i = 1; i < argc; i++) {
        char* arg = argv[i];
        if ((strcmp(arg, "--help") == 0) ||
            (strcmp(arg, "-h") == 0)
        ) {
            print_usage(executable, ctx);
            exit(1);
        }

        if (arg[0] == '-') {
            int value_start = 0;
            int j = 0;
            while (arg[j] != 0) {
                if (arg[j] == '=') {
                    arg[j] = '\0';
                    value_start = j + 1;
                }

                j ++;
            }


            struct named_arg_entry_t* p = ctx->named_list;
            while (p != 0) {
                if (strcmp(p->name, arg) == 0) {

                    if (p->type == ARGUMENT_TYPE_SWITCH) {
                        if (value_start != 0) {
                            fprintf(
                                stderr,
                                "ERROR: Did not expect value for "
                                "argument \"%s\"\n",
                                arg
                            );
                            print_usage(executable, ctx);
                            exit(1);
                        }
                        (*((int*)p->target)) = 1;
                        break;
                    }

                    if (value_start == 0) {
                        i ++;
                    }

                    char* value = argv[i] + value_start;

                    int ret =
                        parse_value(value, p->type, p->target);

                    if (ret == -1) {
                        fprintf(
                            stderr,
                            "ERROR: Failed to parse argument "
                            "\"%s\" with value of \"%s\"\n",
                            arg,
                            value
                        );

                        print_usage(executable, ctx);
                        exit(1);
                    }

                    break;
                }

                p = p->next;
            }

            if (p == NULL) {
                fprintf(
                    stderr,
                    "ERROR: Unknown argument \"%s\"\n",
                    arg
                );
                print_usage(executable, ctx);
                exit(1);
            }
        } else {
            if (pos == 0) {
                fprintf(
                    stderr,
                    "ERROR: Unexpected positional argument \"%s\" "
                    "\n",
                    arg
                );
                print_usage(executable, ctx);
                exit(1);
            }

            int ret = parse_value(arg, pos->type, pos->target);

            if (ret == -1) {
                fprintf(
                    stderr,
                    "ERROR: Unable to parse argument \"%s\" "
                    "with value of \"%s\"\n",
                    pos->name,
                    arg
                );
                print_usage(executable, ctx);
                exit(1);
            }

            pos_index ++;
            pos = pos->next;
        }
    }

    if (pos != 0) {
        fprintf(
            stderr,
            "ERROR: Missing positional argument \"%s\"\n",
            pos->name
        );
        print_usage(executable, ctx);
        exit(1);
    }

    return 0;
}

int argumentor_free(argumentor_t* inout_ctx) {
    if (inout_ctx == NULL) {
        return -1;
    }

    argumentor_t a = (*inout_ctx);
    free_list((struct generic_list_entry_t**)&(a->named_list));
    free_list((struct generic_list_entry_t**)&(a->positional_list));

    return 0;
}

