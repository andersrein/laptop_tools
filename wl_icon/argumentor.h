#ifndef __WL_ICON_ARGUMENTOR_H__
#define __WL_ICON_ARGUMENTOR_H__

struct argumentor_ST;

typedef struct argumentor_ST* argumentor_t;

int argumentor_init(argumentor_t* ctx);

int argumentor_register_int(
    argumentor_t ctx,
    const char* long_name,
    const char* short_name,
    const char* description,
    int* target
);

int argumentor_register_string(
    argumentor_t ctx,
    const char* long_name,
    const char* short_name,
    const char* description,
    const char** target
);

int argumentor_register_switch(
    argumentor_t ctx,
    const char* long_name,
    const char* short_name,
    const char* description,
    int* target
);

int argumentor_parse(argumentor_t ctx, int argc, char** argv);

int argumentor_free(argumentor_t* inout_ctx);

#endif

