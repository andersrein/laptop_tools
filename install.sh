#!/bin/bash

set -e

cd wl_icon
./install.sh
cd ..

DATADIR=/usr/share/laptop_tools

if ! [ -d ${DATADIR} ]; then
    mkdir ${DATADIR}
fi

install -m 664 *.png ${DATADIR}/.
install -m 775 volume_tool /usr/bin/.
install -m 775 brightness_tool /usr/bin/.
install -m 775 battery_monitor /usr/bin/.
install -m 644 usr/lib/systemd/user/battery_monitor.service /usr/lib/systemd/user/battery_monitor.service


echo "OK"

